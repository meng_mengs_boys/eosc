package cli

import (
	"gitee.com/meng_mengs_boys/eosc/etcd"
	"gitee.com/meng_mengs_boys/eosc/service"
)

var _ service.CtiServiceServer = (*MasterCliServer)(nil)

type MasterCliServer struct {
	service.UnimplementedCtiServiceServer
	etcdServe etcd.Etcd
}

func NewMasterCliServer(etcdServe etcd.Etcd) *MasterCliServer {
	return &MasterCliServer{etcdServe: etcdServe}
}
