package grpc_context

import (
	"gitee.com/meng_mengs_boys/eosc/eocontext"
)

func Assert(ctx eocontext.EoContext) (IGrpcContext, error) {
	var grpcContext IGrpcContext
	err := ctx.Assert(&grpcContext)
	return grpcContext, err
}
