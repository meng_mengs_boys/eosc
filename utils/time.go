package utils

import (
	"time"

	"gitee.com/meng_mengs_boys/eosc/log"
)

func TimeSpend(name string) func() {
	t := time.Now()
	return func() {
		log.Info("time spend:", name, ":", time.Since(t))
	}
}
